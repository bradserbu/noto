'use strict';

// ** Dependencies
const util = require('util');
const memoize = require('lodash.memoize');
const Path = require('path');
const fs = require('fs');
const callsites = require('callsites');
const EventEmitter = require('events').EventEmitter;
const ConsoleProvider = require('./providers/ConsoleProvider');
const yargs = require('yargs');

// ** Constants
const CWD = process.cwd();
const FILENAME = __filename;
const DEFAULT_LOG_LEVEL = process.env['DEBUG'] ? 'debug' : 'info';
const DEFAULT_LEVELS = ['error', 'warn', 'info', 'debug', 'trace'];
const DEFAULT_PROVIDERS = [ConsoleProvider];

function exists(filename) {
    return fs.existsSync(filename);
}

function packageName(dir = CWD) {

    const filepath = Path.join(dir, 'package.json');

    // Use .name in package.json
    if (exists(filepath)) {
        const packageJson = require(filepath);
        return packageJson.name;
    }
}

const appName = memoize(() => {
    // Use the directory name of CWD if package.json does not exist
    return packageName() || Path.parse(CWD).base;
});

function StackEntry(entry) {

    const filePath = entry.getFileName();
    return {
        filePath: filePath,
        isModule: filePath === 'module.js',
        functionName: entry.getFunctionName(),
        directoryName: Path.parse(filePath).dir,
        line: entry.getLineNumber(),
        column: entry.getColumnNumber()
    };
}

function caller() {

    const stack = callsites();

    // Return the first caller that did not come from this file
    for (let lcv = 0; lcv < stack.length; lcv++) {
        const entry = StackEntry(stack[lcv]);
        const filePath = entry.filePath;

        if (filePath !== FILENAME) {
            return entry;
        }
    }
}

function getLoggerName() {

    const target = caller();

    const app = appName();
    const relativePath = Path.relative(CWD, target.directoryName);
    const filePath = target.filePath;
    const functionName = target.functionName;

    // console.log({
    //     app,
    //     relativePath,
    //     filePath,
    //     functionName
    // });

    // If there is no relative path, then just use the 'appName' as the name of the logger
    // - This happens from (i.e. 'module.js)
    if (!relativePath)
        return app;

    // Add relative path
    const loggerPath = [app, relativePath];

    // Add the name of the file (without extension) as long as it wasn't 'index.js'
    if (filePath && Path.parse(filePath).base !== 'index.js')
        loggerPath.push(Path.parse(filePath).name);

    // Add the name of the function with a '()'
    if (functionName)
        loggerPath.push(functionName + '()');

    const name = Path.join(...loggerPath).replace(/\//ig, ':');
    return name;
}

class Logger extends EventEmitter {
    constructor(name, {log_level, levels = DEFAULT_LEVELS} = {}) {
        super();

        this._name = name;
        this._levels = levels;
        this._level = log_level || yargs.argv['log-level'] || DEFAULT_LOG_LEVEL;

        const logLevelIndex = level => this._levels.indexOf(level);
        const levelEnabled = level => logLevelIndex(level) <= logLevelIndex(this._level);

        // Add method to log by level (eg: .info(), .debug())
        const levelLogger = level => (message, data) => this.log({level, message, data});
        this._levels.forEach(level => {
            if (levelEnabled(level))
                this[level] = levelLogger(level);
            else
                this[level] = () => {
                };
        });

        // Load Providers
        DEFAULT_PROVIDERS.forEach(provider => provider(this));
    }

    get name() {
        return this._name;
    }

    createLogger(name) {

        const baseName = this.name;

        return name
            ? new Logger(baseName + ':' + name)
            : new Logger(baseName);
    }

    log({level, code, message, data}) {

        // console.error({
        //     level,
        //     code,
        //     message,
        //     data
        // });

        // Timestamp in NanoSeconds
        const timestamp = process.hrtime();
        this.emit('log', {
            timestamp: timestamp,
            level: level,
            code: code,
            message: message,
            data: data
        });
    }
}

function createLogger(name, options = {}) {

    name = name || getLoggerName();

    const logger = new Logger(name, options);
    return logger;
}

// ** Exports
module.exports = createLogger;
module.exports.createLogger = createLogger;