'use strict';

// ** Constants
const COLORS = [5, 1, 6, 2, 3, 4];

// ** Dependencies
const util = require('util');
const chalk = require('chalk');
const prettyjson = require('prettyjson');
const stringify = require('fast-safe-stringify');

// ** DEBUG ENVIRONMENT VARIABLE SUPPORT
const MATCH_DEBUG_FLAG = (process.env.DEBUG || '')
    .split(/[\s,]+/)
    .map(name => {
        name = name.replace('*', '.*?');
        return new RegExp('^' + name + '$');
    });

/**
 * Check if a logger is enabled in the DEBUG environment variable
 * @param name
 * @constructor
 */
const IsDebugEnabled = name => process.env.hasOwnProperty('DEBUG') === false || MATCH_DEBUG_FLAG.some(re => re.test(name));

/**
 * Select a color.
 * @param {String} namespace
 * @return {Number}
 * @api private
 */
function PrefixColor(namespace) {
    var hash = 0, i;

    for (i in namespace) {
        hash  = ((hash << 5) - hash) + namespace.charCodeAt(i);
        hash |= 0; // Convert to 32bit integer
    }

    return COLORS[Math.abs(hash) % COLORS.length];
}

/**
 * Build prefix for log messages using colorized version of the logger name
 * @param name
 * @returns {string}
 */
function Prefix(name) {

    // Used colorized prefix
    if (chalk.supportsColor) {
        const color = PrefixColor(name);
        return '  \u001b[3' + color + ';1m' + name + ' ' + '\u001b[0m';
    }

    // No color
    return `  ${name}  `;
}

/**
 * Calculate the difference in in 'ms' between logging messages
 */
let prevTimestamp;
const diff_ms = timestamp => {
    if (!prevTimestamp) {
        prevTimestamp = timestamp;
        return 0;
    }

    // Diff in nano seconds
    const seconds = timestamp[0] - prevTimestamp[0];
    const nano = timestamp[1] - prevTimestamp[1];

    const ms = (seconds * 1000) + (nano * 0.000001);

    prevTimestamp = timestamp;
    return Math.floor(ms);
};

function ConsoleLogger(logger) {

    const logger_name = logger.name;

    if (!IsDebugEnabled(logger_name)) {
        return;
    }

    const prefix = Prefix(logger_name);

    // Write to STDERR
    const writeLine = text => process.stderr.write(`${prefix}${text}\n`);
    // const debug = require('debug')(logger_name);
    // const writeLine = text => debug(text);

    // Write all log messages
    logger.on('log', ({timestamp, level, message = '', data}) => {

        // level = level.toUpperCase();
        level = level;
        data = util.isNullOrUndefined(data)
            ? ''
            : stringify(data);

        const diff = chalk.blue.bold(`+${diff_ms(timestamp)}ms`);
        // const name = chalk.yellow.bold(logger.name);
        const name = PrefixColor(logger.name);

        // Log Level
        switch (level.toUpperCase()) {
            case 'DEBUG':
                // level = (level);
                break;
            case 'INFO':
                level = chalk.green(level);
                message = chalk.bold(message);
                break;
            case 'ERROR':
                level = chalk.red(level);
                message = chalk.red.bold(message);
                data = chalk.red(data);
                break;
            default:
                break;
        }

        writeLine(`${level} ${message} ${data} ${diff}`);
    });
}

// ** Exports
module.exports = ConsoleLogger;