'use strict';

// ** Exports
const Promise = require('bluebird');
const logger = require('../../lib/Logger').createLogger();

function sayHello(name) {
    logger.info('SAY_HELLO', name);

    return Promise
        .delay(100)
        .then(() => (logger.info('Saying hello...'), `Hello, ${name}!`));
}

// ** Exports
module.exports = sayHello;